<?php
/**
 * Plugin Name: Custom Fields for WooCommerce
 * Description: Add custom fields to WooCommerce products
 * Version: 1.0.0
 * Author: Aroorah Global
 * Author URI: https://www.aroorah.com
 * Text Domain: cfwc
*/
