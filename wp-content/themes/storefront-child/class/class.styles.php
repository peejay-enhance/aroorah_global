<?php

class AG_Style {
	
	public function __construct() {
		add_action( 'wp_enqueue_scripts', array( $this, 'ag_enqueue_bootstrap' ), 15 );
		add_action( 'wp_enqueue_scripts', array( $this, 'ag_enqueue_fonts' ), 15 );
		add_action( 'wp_enqueue_scripts', array( $this, 'ag_enqueue_vendors' ), 15 );
		add_action( 'wp_enqueue_scripts', array( $this, 'ag_enqueue_theme_style' ), 15 );
	}
	
	public function ag_enqueue_bootstrap() {
		wp_enqueue_style( 'bootstrap', 'https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css', array(), '4.1.3' );
	}
	
	public function ag_enqueue_fonts() {
		global $wp_version;
		
		wp_enqueue_style( 'fontawesome', 'https://use.fontawesome.com/releases/v5.5.0/css/all.css', array(), '5.5.0' );
		wp_enqueue_style( 'font.roboto', 'https://fonts.googleapis.com/css?family=Roboto:300,400,500,700', array(), $wp_version );
	}
	
	public function ag_enqueue_vendors() {
		global $wp_version;
		
		// Enqueue vendors here
	}
	
	public function ag_enqueue_theme_style() {
		wp_enqueue_style( 'main', AG_CSS . 'main.css', array( 'bootstrap', 'child-style' ), '1.0.0' );
		wp_enqueue_style( 'responsive', AG_CSS . 'responsive.css', array( 'main' ), '1.0.0' );
	}
	
}

new AG_Style;