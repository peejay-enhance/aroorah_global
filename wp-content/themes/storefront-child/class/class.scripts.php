<?php

class AG_Scipt {
	
	public function __construct() {
		add_action( 'wp_enqueue_scripts', array( $this, 'ag_enqueue_jquery' ), 15 );
		add_action( 'wp_enqueue_scripts', array( $this, 'ag_enqueue_bootstrap' ), 15 );
		add_action( 'wp_enqueue_scripts', array( $this, 'ag_enqueue_vendors' ), 15 );
		add_action( 'wp_enqueue_scripts', array( $this, 'ag_enqueue_theme_script' ), 15 );
	}
	
	public function ag_enqueue_jquery() {
		wp_enqueue_script( 'jquery.core', 'https://code.jquery.com/jquery-3.3.1.min.js', array(), '3.3.1', TRUE );
	}
	
	public function ag_enqueue_bootstrap() {
		wp_enqueue_script( 'jquery.popper', 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js', array( 'jquery.core' ), '1.14.3', TRUE );
		wp_enqueue_script( 'bootstrap', 'https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js', array( 'jquery.core', 'jquery.popper' ), '4.1.3', TRUE );
	}
	
	public function ag_enqueue_vendors() {
		global $wp_version;
		
		// Enqueue vendors here.
	}
	
	public function ag_enqueue_theme_script() {
		wp_enqueue_script( 'main', AG_JS . 'main.js', array( 'jquery.core' ), '1.0.0', TRUE );
	}
	
}

new AG_Scipt;