<?php

/**
 * Load Theme Classes
 */
require trailingslashit( get_stylesheet_directory() ) . 'class/init.php';

/**
 * Enqueue Child Style
 */
function child_theme_enqueue_scripts() {
	wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
	wp_enqueue_style( 'child-style', get_stylesheet_directory_uri() . '/style.css', array( 'parent-style', 'storefront-style', 'storefront-woocommerce-style' ), '1.0.0' );
}
add_action( 'wp_enqueue_scripts', 'child_theme_enqueue_scripts' );

/**
 * Add Class to Anchor
 */
function add_menuclass( $ulclass ) {
   return preg_replace( '/<a /', '<a class="nav-link"', $ulclass );
}
add_filter( 'wp_nav_menu', 'add_menuclass' );

/**
 * Remove auto <p> on pages
 */
function remove_pages_autop() {
	if( is_page() ) {
		remove_filter( 'the_content', 'wpautop' );
		remove_filter( 'the_excerpt', 'wpautop' );
	}
}
add_action( 'wp_head', 'remove_pages_autop' );

/**
 * Login / Logout on nav
 */
function ag_loginout_nav( $items, $args ) {
	if( $args->theme_location != 'primary' ) {
		return $items;
	}
	
	if( is_user_logged_in() ) {
		$items .= '<li><a href="' . home_url( '/my-account' ) . '">' . __( 'My Account' ) . '</a></li>';
		$items .= '<li><a href="' . wp_logout_url( home_url( '/' ) ) . '">' . __( 'Sign Out' ) . '</a></li>';
	} else {
		$items .= '<li><a href="' . home_url( '/my-account' ) . '">' . __( 'Sign In' ) . '</a></li>';
	}
	
	return $items;
}
add_filter( 'wp_nav_menu_items', 'ag_loginout_nav', 199, 2 );

/**
 * Remove breadcrumbs
 */
function ag_remove_breadcrumb() {
	remove_action( 'storefront_before_content', 'woocommerce_breadcrumb', 10 );
}
add_action( 'init', 'ag_remove_breadcrumb', 10 );

/**
 * Password validation
 */
function ag_validate_password( $reg_errors, $sanitized_user_login, $user_email ) {
	global $woocommerce;
	extract( $_POST );
	if( strcmp( $password, $password2 ) !== 0 ) {
		return new WP_Error( 'registration-error', __( 'Passwords do not match.', 'woocommerce' ) );
	}
	return $reg_errors;
}
add_filter( 'woocommerce_registration_errors', 'ag_validate_password', 10, 3 );
 
?>