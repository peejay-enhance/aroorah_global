Supro - Minimalist AJAX WooCommerce WordPress Theme
------------------------------

Supro is a a clean & minimal AJAX WooCommerce WordPress Theme for shopping online stores. This theme is suited for a lot of e-commerce website such as fashion store, furniture store, decoration store, etc.

Change log:

Version 1.2.1:
- Update : Compatible with Woo Brands plugin.
- Fix : Title on Page Header not change when select other product category.
- New : Add option to choose the way open sub menu on mobile.

Version 1.2:

- New: Add new Header Layout 6
- New: Add new Topbar Layout, Topbar Background
- New: Add new Home Boxed, Home Simple
- Update: Compatible with Woocommerce 3.5
- Update: WPBakery Page Builder 5.5.5
- Fixed: Some bug about RTL

Version 1.1:

- New: Topbar available.
- New: Add option to change shop columns on mobile.
- Fix: Typography issue.
- Fix: SKU Variable Product issue.
- Fix: Responsive Account Page.

Version 1.0.2:

- New: Support RTL.
- New: Add new Home Shoppable Images.
- Fix: Responsive Menu.

Version 1.0.1:

- Fix: Responsive for the Custom Heading

Version 1.0
- Initial



