<?php
/**
 * Login Form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/form-login.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.5.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

do_action( 'woocommerce_before_customer_login_form' ); ?>

<div class="login-wrapper">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-5">
				<h1 class="login-heading">Use a service below</h1>
				 <?php do_action( 'wordpress_social_login' ); ?>
			</div>
			<div class="col-md-2"><h2 class="login-options">OR</h2></div>
			<div class="col-md-5">
				<h1 class="login-heading">Use your email address</h1>
				
				<?php if ( get_option( 'woocommerce_enable_myaccount_registration' ) === 'yes' ) : ?>
				<ul class="signinup">
					<li><a href="#signup" >Join</a></li>
					<li><a href="#signin" class="active">Sign In</a></li>
				</ul>
				<?php endif; ?>
				
				<div id="signin" class="form-wrapper active">
					<form class="woocommerce-form woocommerce-form-login login" method="post">
					
						<?php do_action( 'woocommerce_login_form_start' ); ?>
						
						<div class="form-group">
							<label for="username">Email Address or Username<span class="required">*</span></label>
							<input type="text" class="woocommerce-Input woocommerce-Input--text input-text form-control" name="username" id="username" autocomplete="username" value="<?php echo ( ! empty( $_POST[ 'username' ] ) ) ? esc_attr( wp_unslash( $_POST[ 'username' ] ) ) : ''; ?>" />
						</div>
						
						<div class="form-group">
							<label for="password">Password <span class="required">*</span></label>
							<input class="woocommerce-Input woocommerce-Input--text input-text form-control" type="password" name="password" id="password" autocomplete="current-password" />
						</div>
						
						<?php do_action( 'woocommerce_login_form' ); ?>
						
						<?php wp_nonce_field( 'woocommerce-login', 'woocommerce-login-nonce' ); ?>
						
						<button type="submit" class="woocommerce-Button button btn btn-block btn-red" name="login" value="<?php esc_attr_e( 'Sign in', 'woocommerce' ); ?>"><?php esc_html_e( 'Log in', 'woocommerce' ); ?></button>
						
						<div class="forgot-password"><a href="<?php echo esc_url( wp_lostpassword_url() ); ?>"><?php esc_html_e( 'Lost your password?', 'woocommerce' ); ?></a></div>
						
					</form>
				</div>
				
				<?php if ( get_option( 'woocommerce_enable_myaccount_registration' ) === 'yes' ) : ?>
				<div id="signup" class="form-wrapper">
					<form method="post" class="woocommerce-form woocommerce-form-register register" <?php do_action( 'woocommerce_register_form_tag' ); ?> >
					
						<?php do_action( 'woocommerce_register_form_start' ); ?>
						
						<div class="form-group">
							<label for="email">Email Address <span class="required">*</span></label>
							<input type="email" class="woocommerce-Input woocommerce-Input--text input-text form-control" name="email" id="reg_email" autocomplete="email" value="<?php echo ( ! empty( $_POST[ 'email' ] ) ) ? esc_attr( wp_unslash( $_POST[ 'email' ] ) ) : ''; ?>" />
						</div>
						
						<div class="form-group">
							<label for="reg_password">Password <span class="required">*</span></label>
							<div class="input-group">
								<span class="input-group-prepend">
									<button class="btn reveal" type="button"><i class="far fa-eye"></i></button>
								</span> 
								<input type="password" class="woocommerce-Input woocommerce-Input--text input-text password form-control" name="password" id="reg_password" autocomplete="new-password" />
							</div>
						</div>
						
						<div class="form-group">
							<label for="reg_password2">Confirm Password <span class="required">*</span></label>
							<div class="input-group">
								<span class="input-group-prepend">
									<button class="btn reveal2" type="button"><i class="far fa-eye"></i></button>
								</span> 
								<input class="woocommerce-Input woocommerce-Input--text input-text password2 form-control" type="password" name="password2" id="reg_password2" value="<?php if ( ! empty( $_POST[ 'password2' ] ) ) echo esc_attr( $_POST[ 'password2' ] ); ?>" />
							</div>
						</div>
						
						<?php do_action( 'woocommerce_register_form' ); ?>
						
						<div class="form-check">
							<input type="checkbox" class="form-check-input" />
							<label class="form-check-label">I'd like to receive News, Offers and Brand information from Aroorah Global</label>
						</div>
						
						<div class="form-check">
							<input type="checkbox" class="form-check-input" />
							<label class="form-check-label">I'd like to join the Aroorah Global Club and earn points and perks for shopping. <a href="#">Learn more</a> | <a href="#">View Terms</a></label>
						</div>
						
						<?php wp_nonce_field( 'woocommerce-register', 'woocommerce-register-nonce' ); ?>
						
						<button type="submit" class="woocommerce-Button button btn btn-block btn-red" name="register" value="<?php esc_attr_e( 'JOIN', 'woocommerce' ); ?>"><?php esc_html_e( 'JOIN', 'woocommerce' ); ?></button>
						
						<?php do_action( 'woocommerce_register_form_end' ); ?>
					
					</form>
				</div>
				<?php endif; ?>
				
			</div>
		</div>
	</div>
</div>

<?php do_action( 'woocommerce_after_customer_login_form' ); ?>
