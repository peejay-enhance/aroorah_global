<?php

/**
 * Load Theme Classes
 */
require trailingslashit( get_stylesheet_directory() ) . 'class/init.php';

/**
 * Enqueue Child Style
 */
function child_theme_enqueue_scripts() {
	wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css', array( 'supro' ), '1.0.0' );
	wp_enqueue_style( 'child-style', get_stylesheet_directory_uri() . '/style.css', array( 'parent-style' ), '1.0.0' );
  //wp_enqueue_script( 'jquery-3.1.1', 'https://code.jquery.com/jquery-3.1.1.min.js' );
	//wp_enqueue_script( 'bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js' );
	wp_enqueue_script( 'custom', get_stylesheet_directory_uri() . '/assets/js/main.js' );
}
add_action( 'wp_enqueue_scripts', 'child_theme_enqueue_scripts' );

/**
 * Add Class to Anchor
 */
function add_menuclass( $ulclass ) {
   return preg_replace( '/<a /', '<a class="nav-link"', $ulclass );
}
add_filter( 'wp_nav_menu', 'add_menuclass' );

/**
 * Remove auto <p> on pages
 */
function remove_pages_autop() {
	if( is_page() ) {
		remove_filter( 'the_content', 'wpautop' );
		remove_filter( 'the_excerpt', 'wpautop' );
	}
}
add_action( 'wp_head', 'remove_pages_autop' );

/**
 * Login / Logout on nav
 */
function ag_loginout_nav( $items, $args ) {
	if( $args->theme_location != 'primary' ) {
		return $items;
	}
	
	if( is_user_logged_in() ) {
		$items .= '<li><a href="' . home_url( '/my-account' ) . '">' . __( 'My Account' ) . '</a></li>';
		$items .= '<li><a href="' . wp_logout_url( home_url( '/' ) ) . '">' . __( 'Sign Out' ) . '</a></li>';
	} else {
		$items .= '<li><a href="' . home_url( '/my-account' ) . '">' . __( 'Sign In' ) . '</a></li>';
	}
	
	return $items;
}
add_filter( 'wp_nav_menu_items', 'ag_loginout_nav', 199, 2 );

/**
 * Remove breadcrumbs
 */
function ag_remove_breadcrumb() {
	remove_action( 'storefront_before_content', 'woocommerce_breadcrumb', 10 );
}
add_action( 'init', 'ag_remove_breadcrumb', 10 );

/**
 * Shipping cost order by cost
 */
 
 
add_filter( 'woocommerce_package_rates' , 'xa_sort_shipping_services_by_cost', 10, 2 );
function xa_sort_shipping_services_by_cost( $rates, $package ) {
	if ( ! $rates )  return;
	
	$rate_cost = array();
	foreach( $rates as $rate ) {
		$rate_cost[] = $rate->cost;
	}
	
	// using rate_cost, sort rates.
	array_multisort( $rate_cost, $rates );
	
	return $rates;
}



/**
 * Password validation
 */
function ag_validate_password( $reg_errors, $sanitized_user_login, $user_email ) {
	global $woocommerce;
	extract( $_POST );
	if( strcmp( $password, $password2 ) !== 0 ) {
		return new WP_Error( 'registration-error', __( 'Passwords do not match.', 'woocommerce' ) );
	}
	return $reg_errors;
}
add_filter( 'woocommerce_registration_errors', 'ag_validate_password', 10, 3 );

function primer_add_wishlist_endpoint() {
  add_rewrite_endpoint( 'wish-list', EP_ROOT | EP_PAGES );
}
 
add_action( 'init', 'primer_add_wishlist_endpoint' );
 
 
/*
 * Register new wishlist endpoint
*/
function primer_wishlist_query_vars( $vars ) {
    $vars[] = 'wish-list';
    return $vars;
}
 
add_filter( 'query_vars', 'primer_wishlist_query_vars', 0 );
/*
 * Change the order of the endpoints that appear in My Account Page - WooCommerce 2.6
 */
function wpb_woo_my_account_order() {
	$myorder = array(
		'dashboard'          => __( 'Dashboard', 'woocommerce' ),
		'orders'             => __( 'Orders', 'woocommerce' ),
		'downloads'          => __( 'Downloads', 'woocommerce' ),
		'wish-list'          => __( 'Wishlist', 'woocommerce' ),
		'edit-address'       => __( 'Addresses', 'woocommerce' ),
		'add-payment-method' => __( 'Payment Method', 'woocommerce' ),
		'edit-account'       => __( 'Account Details', 'woocommerce' ),
		'customer-logout'    => __( 'Logout', 'woocommerce' ),
	);
	return $myorder;
}
add_filter ( 'woocommerce_account_menu_items', 'wpb_woo_my_account_order' );
/*
 * Add Wishlist Shortcode - https://wordpress.org/plugins/ti-woocommerce-wishlist/
*/
function woocommerce_wishlist_content() {
  echo do_shortcode( '[yith_wcwl_wishlist]' );
}
 
add_action( 'woocommerce_account_wish-list_endpoint', 'woocommerce_wishlist_content' );

// Display Fields
add_action('woocommerce_product_options_general_product_data', 'woocommerce_product_custom_fields');

// Save Fields
add_action('woocommerce_process_product_meta', 'woocommerce_product_custom_fields_save');


function woocommerce_product_custom_fields()
{
    global $woocommerce, $post;
    echo '<div class="product_custom_field">';
    // Custom Product Text Field
    woocommerce_wp_text_input(
        array(
            'id'          => '_sale_price_dates_from',
            'type'        => 'text',
            'class'       => 'short hasDatepicker',
            'name'        => '_sale_price_dates_from',
            'maxlength'   => '10',
            'pattern'     => '[0-9]{4}-(0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[01])',
            'placeholder' => 'From… YYYY-MM-DD',
            'label'       => __('Survey Date Start', 'woocommerce'),
            //'desc_tip'    => 'true'
        )
    );
    woocommerce_wp_text_input(
        array(
            'id'          => '_survey_dates_to',
            'type'        => 'text',
            'class'       => 'datepicker',
            'default-value' => '<?php echo date("Y/m/d"); ?>',
            'pattern'     => '[0-9]{4}-(0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[01])',
            'placeholder' => 'To… YYYY-MM-DD',
            'label'       => __('Survey Date Ending', 'woocommerce') 
            //'desc_tip'    => 'true'
        )
    );
    //Custom Product Number Field
    woocommerce_wp_text_input(
        array(
            'id' 				=> '_custom_product_number_field',
            'placeholder' 		=> 'Number of customers',
            'label' 			=> __('Goal 1', 'woocommerce'),
            'type' 				=> 'number',
            'custom_attributes' => array(
            'step' 				=> 'any',
            'min' 				=> '0'
            )
        )
    );
    woocommerce_wp_text_input(
        array(
            'id' 				=> '_custom_product_number_field',
            'placeholder' 		=> 'Number of customers',
            'label' 			=> __('Goal 2', 'woocommerce'),
            'type' 				=> 'number',
            'custom_attributes' => array(
            'step' 				=> 'any',
            'min' 				=> '0'
            )
        )
    );
    woocommerce_wp_text_input(
        array(
            'id'  				=> '_custom_product_number_field',
            'placeholder' 		=> 'Number of customers',
            'label' 			=> __('Goal 3', 'woocommerce'),
            'type' 				=> 'number',
            'custom_attributes' => array(
            'step' 				=> 'any',
            'min'  				=> '0'
            )
        )
    );
    woocommerce_wp_textarea_input(
        array(
            'id' 		  => '_custom_product_textarea',
            'placeholder' => 'Notes and description',
            'label' 	  => __('Notes', 'woocommerce')
        )
    );
    
    echo '</div>';

}

function woocommerce_product_custom_fields_save($post_id)
{
    // Custom Product Text Field
    $woocommerce_custom_product_text_field = $_POST['_custom_product_text_field'];
    if (!empty($woocommerce_custom_product_text_field))
        update_post_meta($post_id, '_custom_product_text_field', esc_attr($woocommerce_custom_product_text_field));
	// Custom Product Number Field
    $woocommerce_custom_product_number_field = $_POST['_custom_product_number_field'];
    if (!empty($woocommerce_custom_product_number_field))
        update_post_meta($post_id, '_custom_product_number_field', esc_attr($woocommerce_custom_product_number_field));
	// Custom Product Textarea Field
    $woocommerce_custom_procut_textarea = $_POST['_custom_product_textarea'];
    if (!empty($woocommerce_custom_procut_textarea))
        update_post_meta($post_id, '_custom_product_textarea', esc_html($woocommerce_custom_procut_textarea));

}
 

/**
 * @snippet       Change "Add to Cart" Button Label if Product Already @ Cart
  // Part 1
  // Edit Single Product Page Add to Cart
 */
 

 
add_filter( 'woocommerce_product_single_add_to_cart_text', 'bbloomer_custom_add_cart_button_single_product' );
 
function bbloomer_custom_add_cart_button_single_product( $label ) {
     
    foreach( WC()->cart->get_cart() as $cart_item_key => $values ) {
        $product = $values['data'];
        if( get_the_ID() == $product->get_id() ) {
            $label = __('Already in Cart. Add again?', 'woocommerce');
        }
    }
     
    return $label;
 
}
 
// Part 2
// Edit Loop Pages Add to Cart
 
add_filter( 'woocommerce_product_add_to_cart_text', 'bbloomer_custom_add_cart_button_loop', 99, 2 );
 
function bbloomer_custom_add_cart_button_loop( $label, $product ) {
     
    if ( $product->get_type() == 'simple' && $product->is_purchasable() && $product->is_in_stock() ) {
         
        foreach( WC()->cart->get_cart() as $cart_item_key => $values ) {
            $_product = $values['data'];
            if( get_the_ID() == $_product->get_id() ) {
                $label = __('Already in Cart. Add again?', 'woocommerce');
            }
        }
         
    }
     
    return $label;
     
}

add_action( 'woocommerce_before_checkout_form', 'bbloomer_cart_on_checkout_page_only', 5 );
 
function bbloomer_cart_on_checkout_page_only() {
 
if ( is_wc_endpoint_url( 'order-received' ) ) return;
 
// NOTE: I had to change the name of the shortcode below...
// ...as it would have displayed this site's Cart...
// ... make sure to use "woocommerce_cart" inside "[]":
 
echo do_shortcode('[woocommerce_cart]');
 
}

/**
 * Add to cart redirect to checkout.
 * 
 * @param string $url
 * @return string
 */
function my_wc_add_to_cart_redirect_to_checkout( $url ) {
	return wc_get_checkout_url();
}

add_filter( 'woocommerce_add_to_cart_redirect', 'my_wc_add_to_cart_redirect_to_checkout' );


////////////////////////////////////


function prefix_add_text_input() {
  $args = array(
    'label' => 'Goal 1', // Text in the label in the editor.
    'placeholder' => 'Number of customers', // Give examples or suggestions as placeholder
    'class' => '',
    'style' => '',
    'wrapper_class' => '',
    'value' => '', // if empty, retrieved from post_meta
    'id' => '', // required, will be used as meta_key
    'name' => '', // name will be set automatically from id if empty
    'type' => '',
    'desc_tip' => '',
    'data_type' => 'int',
    'custom_attributes' => '', // array of attributes you want to pass 
    'description' => ''
  );
  woocommerce_wp_text_input( $args );

}
add_action( 'woocommerce_product_options_pricing', 'prefix_add_text_input' );


function prefix_add_radio_buttons() {
  $args = array(
    'label' => '', // Text in the label in the editor
    'class' => 'prefix_radio', // custom class
    'style' => '', 
    'wrapper_class' => '', // custom wrapper class
    'value' => '', // if empty, retrieved from meta_value where id is the meta_key
    'id' => '', // required, will be used as meta_key for this field
    'name' => 'my_radio_buttons', // required if you want only one option to be selected at a time
    'options' => array( // options for radio inputs, array
      'value1' => __( 'Proceed to regular purchase if goal is not met' ),
      'value2' => __( 'Ask for a refund if goal is not met', 'your_text_domain' )
    ), 
    'desc_tip' => '', // true or false. Controls whether your description is shown directly or as tooltip
    'description' => '' // provide a useful description
  );
  woocommerce_wp_radio( $args );
}
add_action( 'woocommerce_product_options_tax', 'prefix_add_radio_buttons' );

// Add a custom tab in edit product page settings
add_filter( 'woocommerce_product_data_tabs', 'product_data_tab', 90 , 1 );
function product_data_tab( $product_data_tabs ) {
    $product_data_tabs['shipping-costs'] = array(
        'label' => __( 'Promotion Details', 'my_theme_domain' ), // translatable
        'target' => 'shipping_costs_product_data', // translatable
    );
    return $product_data_tabs;
}

// Add the content to the custom tab in edit product page settings
add_action( 'woocommerce_product_data_panels', 'product_data_fields' );
function product_data_fields() {
    global $post;

    echo '<style>fieldset.form-field._input_radio_field > legend{float:none;}</style>
        <div id="shipping_costs_product_data" class="panel woocommerce_options_panel">';

    // Checkbox field 1
    woocommerce_wp_checkbox( array(
        'id'            => '_input_checkbox1',
        'wrapper_class' => 'show_if_simple',
        'label'         => __( 'Goal 1 customers (1,000) to give 5% Discount' ),
        'description'   => __( 'Input checkbox Description 1' )
    ) );

    // Checkbox field 2
    woocommerce_wp_checkbox( array(
        'id'            => '_input_checkbox2',
        'wrapper_class' => 'show_if_simple',
        'label'         => __( 'Goal 2 customers (3,000) to give 10% Discount' ),
        'description'   => __( 'Input checkbox Description 2' )
    ) );

    // Checkbox field 3
    woocommerce_wp_checkbox( array(
        'id'            => '_input_checkbox3',
        'wrapper_class' => 'show_if_simple',
        'label'         => __( 'Goal 3 customers (5,000) to give 15% Discount' ),
        'description'   => __( 'Input checkbox Description 3' )
    ) );

    // Radio Buttons field
    woocommerce_wp_radio( array(
        'id'            => '_input_radio',
        'wrapper_class' => 'show_if_simple',
        'label'         => __('Delivery Period'),
        'description'   => __( 'Delivery Period Description' ).'<br>',
        'options'       => array(
            '-5 days'       => __('Less than 5 days'),
            '10 days'       => __('10 days'),
            '15 days'       => __('15 days'),
            '30 days'       => __('30 days'),
        )
    ) );

    echo '</div>';
}

// Save the data of the custom tab in edit product page settings
add_action( 'woocommerce_process_product_meta',   'shipping_costs_process_product_meta_fields_save' );
function shipping_costs_process_product_meta_fields_save( $post_id ){
    // save the checkbox field 1 data
    $wc_checkbox = isset( $_POST['_input_checkbox1'] ) ? 'yes' : 'no';
    update_post_meta( $post_id, '_input_checkbox1', $wc_checkbox );

    // save the checkbox field 2 data
    $wc_checkbox = isset( $_POST['_input_checkbox2'] ) ? 'yes' : 'no';
    update_post_meta( $post_id, '_input_checkbox2', $wc_checkbox );

    // save the checkbox field 3 data
    $wc_checkbox = isset( $_POST['_input_checkbox3'] ) ? 'yes' : 'no';
    update_post_meta( $post_id, '_input_checkbox3', $wc_checkbox );

    // save the radio button field data
    $wc_radio = isset( $_POST['_input_radio'] ) ? $_POST['_input_radio'] : '';
    update_post_meta( $post_id, '_input_radio', $wc_radio );
}


## ---- 2. Front end ---- ##

// Add a custom tab in single product pages
add_filter( 'woocommerce_product_tabs', 'custom_product_tab' );
function custom_product_tab( $tabs ) {

    $tabs['cust_tab'] = array(
        'title'     => __( 'Promotion Details', 'woocommerce' ),
        'priority'  => 50,
        'callback'  => 'custom_product_tab_content',
    );

    return $tabs;
}

// Add the content inside the custom tab in single product pages
function custom_product_tab_content()  {
    global $post;

    $output = '<div class="custom-data">';

    ## 1. The Chekboxes (3)

    $checkbox1 = get_post_meta( $post->ID, '_input_checkbox1', true ); // Get the data - Checbox 1
    if( ! empty( $checkbox1 ) && $checkbox1 == 'yes' )
        $text_to_display = __('is "Included in the Promotion"');
    else
        $text_to_display = __('is "NOT Included in the Promotion"');
    $output .= '<p>'. __('Goal 1 customers (1,000) to give 5% Discount:').' <span style="color:#96588a;">'.$text_to_display.'</span></p>';

    $checkbox2 = get_post_meta( $post->ID, '_input_checkbox2', true ); // Get the data - Checbox 2
    if( ! empty( $checkbox2 ) && $checkbox2 == 'yes' )
        $text_to_display = __('is "Included in the Promotion"');
    else
        $text_to_display = __('is "NOT Included in the Promotion"');
    $output .= '<p>'. __('Goal 2 customers (3,000) to give 10% Discount:').' <span style="color:#96588a;">'.$text_to_display.'</span></p>';

    $checkbox3 = get_post_meta( $post->ID, '_input_checkbox3', true ); // Get the data - Checbox 3
    if( ! empty( $checkbox3 ) && $checkbox3 == 'yes' )
        $text_to_display = __('is "Included in the Promotion"');
    else
        $text_to_display = __('is "NOT Included in the Promotion"');
    $output .= '<p>'. __('Goal 3 customers (5,000) to give 15% Discount:').' <span style="color:#96588a;">'.$text_to_display.'</span></p>';

    ## 2. Radio buttons

    $radio = get_post_meta( $post->ID, '_input_radio', true ); // Get the data
    if( ! empty( $radio ) )
        $output .= '<p>'. __('Delivery Period: ').'<span style="color:#96588a;">'.$radio.'</span></p>';
    else
        $output .= '<p>'. __('Delivery Period: ').'<span style="color:#96588a;">'.__('Not defined').'</span></p>';

    echo $output.'</div>';
}

 
function bbloomer_product_sold_count() {
global $product;
$units_sold = get_post_meta( $product->get_id(), 'total_sales', true );
if ( $units_sold ) echo '<p>' . sprintf( __( 'Units Sold: %s', 'woocommerce' ), $units_sold ) . '</p>';
}