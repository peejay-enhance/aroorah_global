<?php

/*==================================
DEFINE THEME DIRECTORIES
==================================*/
/* Root Directory */
DEFINE( 'AG_ROOT',		trailingslashit( get_stylesheet_directory() ) );
DEFINE( 'AG_ROOT_URI',	trailingslashit( get_stylesheet_directory_uri() ) );

/* Assets Directory */
DEFINE( 'AG_CSS',		trailingslashit( AG_ROOT_URI . 'assets/css' ) );
DEFINE( 'AG_JS',		trailingslashit( AG_ROOT_URI . 'assets/js' ) );
DEFINE( 'AG_IMG',		trailingslashit( AG_ROOT_URI . 'assets/img' ) );
DEFINE( 'AG_FONTS',		trailingslashit( AG_ROOT_URI . 'assets/fonts' ) );
DEFINE( 'AG_VENDORS',	trailingslashit( AG_ROOT_URI . 'assets/vendors' ) );

/*==================================
LOAD THEME CLASSES
==================================*/
/* Classes */
REQUIRE_ONCE( AG_ROOT . 'class/class.styles.php' );
REQUIRE_ONCE( AG_ROOT . 'class/class.scripts.php' );
REQUIRE_ONCE( AG_ROOT . 'class/class.shortcodes.php' );

/* Templates */
REQUIRE_ONCE( AG_ROOT . 'class/template/class.template.php' );
REQUIRE_ONCE( AG_ROOT . 'class/template/class.header.php' );
REQUIRE_ONCE( AG_ROOT . 'class/template/class.banner.php' );
REQUIRE_ONCE( AG_ROOT . 'class/template/class.navigation.php' );
REQUIRE_ONCE( AG_ROOT . 'class/template/class.footer.php' );