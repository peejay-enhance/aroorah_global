<?php

class AG_Styles {
	
	public function __construct() {
		add_action( 'wp_enqueue_scripts', array( $this, 'ag_enqueue_bootstrap' ), 15 );
		add_action( 'wp_enqueue_scripts', array( $this, 'ag_enqueue_fonts' ), 15 );
		add_action( 'wp_enqueue_scripts', array( $this, 'ag_enqueue_vendors' ), 15 );
		add_action( 'wp_enqueue_scripts', array( $this, 'ag_enqueue_theme_style' ), 15 );
	}
	
	public function ag_enqueue_bootstrap() {
		wp_deregister_style( 'bootstrap' );
		wp_enqueue_style( 'bootstrap', 'https://www.aroorah.com/wp-content/themes/supro-child/assets/addons/bootstrap.min.css', array(), '4.1.3' );
	}
	
	public function ag_enqueue_fonts() {
		global $wp_version;
		
		wp_enqueue_style( 'fontawesome', 'https://use.fontawesome.com/releases/v5.6.1/css/all.css', array(), '5.6.1' );
	}
	
	public function ag_enqueue_vendors() {
		global $wp_version;
		
		wp_enqueue_style( 'slick', '//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css', array(), '1.8.1' );
	}
	
	public function ag_enqueue_theme_style() {
		wp_enqueue_style( 'main', AG_CSS . 'main.css', array( 'child-style', 'bootstrap', 'wsocial' ), '1.0.0' );
		wp_enqueue_style( 'responsive', AG_CSS . 'responsive.css', array( 'main' ), '1.0.0' );
	}
	
}

new AG_Styles;