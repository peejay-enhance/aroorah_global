/*==================================
SIGN IN / SIGN UP
==================================*/
jQuery(document).ready(function($){
	$('ul.signinup>li>a').on('click', function(event){
		event.preventDefault();
		var href = $(this).attr('href');
		
		$('ul.signinup>li>a').removeClass('active');
		$(this).addClass('active');
		
		$('.form-wrapper').removeClass('active');
		$(href).addClass('active');
	});
});



/*==================================
SHOW / HIDE PASSWORD
==================================*/
jQuery(document).ready(function($){
	$('.reveal').on('click', function(event){
		event.preventDefault();
		
		var pwd = $('.password');
		
		if(pwd.attr('type')=='password') {
			pwd.attr('type','text');
		} else {
			pwd.attr('type','password');
		}
	});
	
	$('.reveal2').on('click', function(event){
		event.preventDefault();
		
		var pwd2 = $('.password2');
		
		if(pwd2.attr('type')=='password') {
			pwd2.attr('type','text');
		} else {
			pwd2.attr('type','password');
		}
	});
  
  /*==================================
Disabling fields from checkout page
==================================*/
  jQuery(document).ready(function($){
  $( "#select2-billing_country-container" ).prop( "disabled", true );
  //$( "#billing_city" ).prop( "disabled", true );
  //$( "#billing_state" ).prop( "disabled", true );
 // $( "#billing_postcode" ).prop( "disabled", true );
  });
  

	
});


